﻿using System;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Module4 Task1 = new Module4();
            int[] arrTask1 = new int[] { -1, 12, 23, 94, -65 };
            int[] arrTask1E = new int[] { -1, 12, 23, 94, -65 };
            int res_Task_1_A = Task1.Task_1_A(arrTask1);
            int res_Task_1_B = Task1.Task_1_B(arrTask1);
            int res_Task_1_C = Task1.Task_1_C(arrTask1);
            int res_Task_1_D = Task1.Task_1_D(arrTask1);
            Task1.Task_1_E(arrTask1E);
            Console.WriteLine(res_Task_1_A);
            Console.WriteLine(res_Task_1_B);
            Console.WriteLine(res_Task_1_C);
            Console.WriteLine(res_Task_1_D);
        }


        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Array.Sort(array);
            return array[array.Length - 1];                      
        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Array.Sort(array);
            return array[0];
        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int result = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                result += array[i];
            }
            return result;
        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            Array.Sort(array);
            return array[array.Length - 1] - array[0];
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException(nameof(array));
            }

            int[] newArr = new int[array.Length];
            Array.Copy(array, 0 , newArr, 0, array.Length);
            Array.Sort(newArr);
            for (int i = 0; i < array.Length; i++)
            {                
                if (i % 2 == 0)
                {
                    array[i] += newArr[newArr.Length - 1];
                }
                else
                {
                    array[i] -= newArr[0];
                }
            }

        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return string.Concat(a, b);
        }

        public int[] Task_2(int[] a, int[] b)
        {
            if (a.Length > b.Length)
            {
                for (int i = 0; i < b.Length; i++)
                {
                    a[i] += b[i];
                }
            }

            else if (b.Length > a.Length)
            {
                for (int i = 0; i < a.Length; i++)
                {
                    b[i] += a[i];                    
                }
                return b;
            }

            else
            {
                for (int i = 0; i < a.Length; i++)
                {
                    a[i] += b[i];
                }
            }
            return a;
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentException("Argument is negative.");
            }
            length = 2 * Math.PI * radius;
            square = Math.PI * Math.Pow(radius, 2);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            sumOfItems = array[0];            
            for (int i = 1; i < array.Length; i++)
            {
                sumOfItems += array[i];
            }
            Array.Sort(array);
            maxItem = array[array.Length - 1];
            minItem = array[0];
        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            return (numbers.Item1 + 10, numbers.Item2 + 10, numbers.Item3 + 10);
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentException("Argument is negative.");
            }
            return (2 * Math.PI * radius, Math.PI * Math.Pow(radius, 2));
        }

        public (int, int, int) Task_4_C(int[] array)
        {            
            int min = 0;
            int max = 0;
            int sum = array[0];
            for (int i = 1; i < array.Length; i++)
            {
                sum += array[i];
            }
            Array.Sort(array);
            max = array[array.Length - 1];
            min = array[0];
            return (min, max, sum);
        }

        public void Task_5(int[] array)
        {
            if (array.Length == 0)
            {
                throw new ArgumentException("Null argument.");
            }
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array.Length == 0)
            {
                throw new ArgumentException("Null argument.");
            }

            else if (direction == SortDirection.Ascending)
            {
                Array.Sort(array);
            }

            else if (direction == SortDirection.Descending)
            {
                Array.Sort(array);
                Array.Reverse(array);
            }
        }        

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {
            if (Math.Abs(x1 - x2) / Math.Abs(x2) >= e)
            {
                double midSegment = (x1 + x2) / 2;

                if (func(midSegment) * func(x1) < 0)
                {
                    x2 = midSegment;
                }

                else
                {
                    x1 = midSegment;
                }
                return Task_7(func, x1, x2, e, result);
            }
            return (x1 + x2) / 2;
        }
    }
}
